# README #

This the docker-compose.yml file and underlying docker build files.  For linux- I had to install docker and then add docker-compose tool.  The Mac download of Docker should have both docker and docker-compose inlcuded. 

### This exists to help quickly bring up the pieces (database - wiki etc) ###

* install docker and docker-compose
* create the folder under the user's home directary - ~/docker-data/wikidata
* cd into ~/docker-data/wikidata and run git init (gollum uses GIT for storing files) - Note: it will work without this - but does not persist the data
* cd into root dir - and use "docker-compose up"
* This will run mongodb on it's default ports, gollum and a QR Generator (all pulled from docker images)
* At the moment - this runs all the pieces except for the Golang router

Once up - you should be able to log into mongo (I did install the mongo client locally - without docker).
gollum is running on port 81 (http://localhost:81)

The qr generator notes are here https://github.com/samwierema/go-qr-generator
Navigating to - should produce a QR Code
http://localhost:8080/?data=Hello%2C%20world&size=300


### Notes ###
* The docker-compose files refers to 2 Docker build files in their respective folder.  As these images are built from docker-hub - they are otherwise empty. 

* See image notes in hub.docker.com - for details about particular images

* Components
  qrrouter - The Golang router/forwarder to take users to QR REsources
  qrwebapi - microservices app for the management
  Gollum - Githubs GOllum wiki - likely our main wiki for publishing QR Resources
  QRGenerator - a golang program to generate QR codes from web requests
  documentation is here https://github.com/samwierema/go-qr-generator







### Contribution guidelines ###

Connecting to a container - running
docker ps - will list the containers
AND
docker exec -i -t num /bin/bash

REBUILDING DOCKER images
docker-compose build --no-cache qrrouter
