docker run \
            -e MONGO_HOST=localhost \
            -e MONGO_PORT=27017 \
            -e MONGO_DB_NAME=test2 \
            -e MONGO_COLLECTION_NAME=your_collection_name \
            bwnyasse/docker-mongodb-worker \
            /start.sh -i
